# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import datetime, timedelta, time

from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.pyson import Eval
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.report import Report


class ShiftKind(ModelSQL, ModelView):
    "Shift Kind"
    __name__ = "staff.shift.kind"
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    start = fields.Time('Start', required=True)
    end = fields.Time('End', required=True)
    total_time = fields.Function(fields.Float('Total Time'), 'get_total_time')
    legal_work_time = fields.Float('Legal Work Time', digits=(2, 2))
    mode = fields.Selection([
        ('work', 'Work'),
        ('event', 'Event'),
        ('rest', 'Rest'),
        ('none', 'None'),
    ], 'Mode', required=True)
    event_category = fields.Many2One('staff.event_category', 'Event')

    @classmethod
    def __setup__(cls):
        super(ShiftKind, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))

    @staticmethod
    def default_legal_work_time():
        return 8

    @staticmethod
    def default_mode():
        return 'work'

    def get_total_time(self, name):
        if self.start and self.end:
            t1 = timedelta(hours=self.start.hour, minutes=self.start.minute)
            t2 = timedelta(hours=self.end.hour, minutes=self.end.minute)
            return round((t2 - t1).seconds / 3600, 2)
        return 0


class Shift(Workflow, ModelSQL, ModelView):
    "Staff Shift"
    __name__ = 'staff.shift'
    STATES = {'readonly': Eval('state') != 'draft'}

    shift_date = fields.Date('Shift Date', required=True, states=STATES)
    planned_date = fields.Date('Planned Date', readonly=True,
            states=STATES)
    section = fields.Many2One('staff.section', 'Section',
            required=True, states=STATES)
    lines = fields.One2Many('staff.shift.line', 'shift', 'Lines',
            states=STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
            ], 'State', readonly=True)

    @classmethod
    def __setup__(cls):
        super(Shift, cls).__setup__()
        cls._order.insert(0, ('shift_date', 'DESC'))
        cls._transitions |= set((
                ('draft', 'confirmed'),
                ('confirmed', 'draft'),
                ))
        cls._buttons.update({
                'draft': {
                    'invisible': Eval('state') == 'draft',
                    },
                'confirm': {
                    'invisible': Eval('state') != 'draft',
                    },
                'add_shift': {
                    'invisible': Eval('state') != 'draft',
                    },
        })

    def get_rec_name(self, name):
        return '[' + str(self.shift_date) + '] ' + self.section.name

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_planned_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, records):
        pass

    @classmethod
    @ModelView.button_action('staff_access.wizard_add_shift')
    def add_shift(cls, records):
        pass


class ShiftLine(ModelSQL, ModelView):
    "Staff Shift Line"
    __name__ = 'staff.shift.line'
    shift = fields.Many2One('staff.shift', 'Shift', required=True)
    employee = fields.Many2One('company.employee', 'Employee',
            required=True)
    enter_timestamp = fields.DateTime('Enter', required=True)
    exit_timestamp = fields.DateTime('Exit', required=True)

    def get_rec_name(self, name):
        return (self.shift.rec_name or ' [ ' + str(self.id)
            + ' ] ' + str(self.enter_timestamp) + ' - ' + str(self.exit_timestamp))


class StaffSection(ModelSQL, ModelView):
    "Staff Section"
    __name__ = 'staff.section'
    name = fields.Char('Name', required=True)
    employees = fields.Many2Many('staff.section-company.employee',
            'section', 'employee', 'Employees')


class SectionEmployee(ModelSQL):
    'Section - Employee'
    __name__ = 'staff.section-company.employee'
    section = fields.Many2One('staff.section', 'Section', select=True,
            required=True, ondelete='CASCADE')
    employee = fields.Many2One('company.employee', 'Employee',
            select=True, required=True, ondelete='CASCADE')


class AddShiftStart(ModelView):
    'Add Shift Start'
    __name__ = 'staff.add_shift.start'
    employees = fields.Many2Many('company.employee', None, None,
            'Employees', required=True)
    enter_hour = fields.Time('Enter Hour', required=True)
    timework = fields.Float('Time Work', required=True)


class AddShift(Wizard):
    'Add Shift'
    __name__ = 'staff.add_shift'
    start = StateView('staff.add_shift.start',
        'staff_access.add_shift_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def convert_seconds(self, value):
        hour = int(value)
        minute = (float(value) - int(value)) * 3600
        return hour, minute

    def transition_accept(self):
        ids = Transaction().context['active_ids']
        Shift = Pool().get('staff.shift')
        if ids:
            shift = Shift(ids[0])
            lines_to_create = []
            enter_hour_tz = time(
                    self.start.enter_hour.hour + 5,
                    self.start.enter_hour.minute)
            enter_ = datetime.combine(shift.shift_date, enter_hour_tz)
            exit_ = enter_ + timedelta(0, self.start.timework * 3600)
            for e in self.start.employees:
                lines_to_create.append({
                    'employee': e.id,
                    'enter_timestamp': enter_,
                    'exit_timestamp': exit_,
                })

            Shift.write([shift], {
                'lines': [('create', lines_to_create)],
                }
            )
        return 'end'


class ShiftSheetReport(Report):
    "Shift Sheet Report"
    __name__ = 'staff_access.shift_sheet_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Company = Pool().get('company.company')
        report_context['company'] = Company(Transaction().context['company'])
        return report_context
