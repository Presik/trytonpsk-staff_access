# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import access
from . import shift


def register():
    Pool.register(
        shift.StaffSection,
        shift.Shift,
        shift.ShiftKind,
        shift.ShiftLine,
        access.StaffAccess,
        shift.SectionEmployee,
        shift.AddShiftStart,
        access.ReportHoursStart,
        access.AddAccessGroupStart,
        module='staff_access', type_='model')
    Pool.register(
        shift.AddShift,
        access.ReportHoursWiz,
        access.AddAccessGroup,
        module='staff_access', type_='wizard')
    Pool.register(
        access.ReportHours,
        shift.ShiftSheetReport,
        module='staff_access', type_='report')
